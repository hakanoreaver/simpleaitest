

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.JButton;
import javax.swing.JLabel;

public class Board extends JPanel implements ActionListener{


    private static ArrayList<Tree> trees;
    private final int B_WIDTH = 1100;
    private final int B_HEIGHT = 600;
    private final int DELAY = 1;
   // private final int[][] pos = {

       //     {780, 109}, {580, 139}, {680, 239},
       //     {790, 259}, {760, 50}, {790, 150},
       //     {980, 209}, {560, 45}, {510, 70},
       //     {930, 159}, {590, 80}, {530, 60},
      //      {940, 59}, {990, 30}, {920, 200},
     //       {900, 259}, {660, 50}, {540, 90},
     //       {810, 220}, {860, 20}, {740, 180},
     //       {820, 128}, {490, 170}, {700, 30}
   // };

    private House house;
    private Timer timer;
    private boolean ingame;

    private int touchInt;
    private int wood = 0;



    public Board() {
        initBoard();
    }

    public static ArrayList getTrees() {
        return trees;
    }

    private void initBoard() {
        ingame = true;

        setFocusable(true);
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
        this.setLayout(null);

        JButton btnAddForester = new JButton("Buy Forester");
        btnAddForester.setBounds(60, 400, 200, 30);
        btnAddForester.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (wood >= 30) {
                    wood -= 30;
                    house.spawnForester();
                }
            }
        });
        this.add(btnAddForester);
        btnAddForester.setLocation(610, 0);

        JButton btnAddPerson = new JButton("Buy Person");
        btnAddPerson.setBounds(0,500, 200, 30);
        btnAddPerson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (wood >= 10) {
                    wood -= 10;
                    house.spawnPerson();
                }
            }
        });
        this.add(btnAddPerson);
        btnAddPerson.setLocation(400,0);

       JLabel personCost = new JLabel("Cost: 30 Wood");
       personCost.setBounds(0, 610, 200, 30);
       this.add(personCost);
        personCost.setLocation(610,40);

        JLabel foragerCost = new JLabel("Cost: 10 Wood");
        foragerCost.setBounds(400, 40, 200, 30);
        this.add(foragerCost);

        initTrees();

        house = new House(120,120);
        initPeople();
        timer = new Timer(DELAY, this);
        timer.start();


    }

    public void initPeople() {
        house.spawnPerson();
        house.spawnForester();
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (ingame) {

            drawObjects(g);

        } else {

            drawGameOver(g);

        }

        Toolkit.getDefaultToolkit().sync();
    }

//TODO add in collisions to take cut down trees

    private void initTrees() {

        trees = new ArrayList<>();



        for (int i = 0; i <= 50; i++) {

            int x = ThreadLocalRandom.current().nextInt(0, 1001);
            int y = ThreadLocalRandom.current().nextInt(80, 501);
            trees.add(new Tree(x, y));
        }
    }

    private void checkCollisions() {

        ArrayList<Person> ms = house.getPeople();

        for (Person m : ms) {

            Rectangle r1 = m.getBounds();

            Rectangle r3 = house.getBounds();

            if (r1.intersects(r3)) {



                if (m.getHouseTouch() == false && m.getWithTree() == true) {
                    m.setWithTreeFalse();
                    m.setHouseTouchTrue();
                    m.moveToTree();

                    wood++;
                }


            }

            for (Tree tree : trees) {

                Rectangle r2 = tree.getBounds();

                if (r1.intersects(r2) && m.getWithTree() == false && tree.isTaken() == true && m.getPersonNo() == tree.getChoppableBy()) {
                    tree.setVisible(false);

                    m.setWithTreeTrue();
                    m.moveToHouse();
                    m.setHouseTouchFalse();
                }
            }
        }


    }

    private void drawObjects(Graphics g) {

        if (house.isVisible()) {
            g.drawImage(house.getImage(), house.getX(), house.getY(),
                    this);
        }

        for (Tree m : trees) {
            if (m.isVisible()) {
              g.drawImage(m.getImage(), m.getX(), m.getY(), this);
            }
        }

        ArrayList<Person> ms = house.getPeople();

        for (Person m : ms) {
            if (m.isVisible()) {
                g.drawImage(m.getImage(), m.getX(), m.getY(), this);
            }
        }

        ArrayList<Forester> fs = house.getForesters();

        for (Forester f : fs) {
            if (f.isVisible()) {
                g.drawImage(f.getImage(), f.getX(), f.getY(), this);
            }
        }
        g.setColor(Color.BLACK);
        g.drawString("Wood: " + Integer.toString(wood), 5, 15);

        g.setColor(Color.BLACK);
        g.drawString(Integer.toString(touchInt), 15, 60);

    }

    private void drawGameOver(Graphics g) {

        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics fm = getFontMetrics(small);

        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msg, (B_WIDTH - fm.stringWidth(msg)) / 2,
                B_HEIGHT / 2);


    }








    private void updatePeople() {

        ArrayList<Person> ms = house.getPeople();

        for (int i = 0; i < ms.size(); i++) {

            Person m = ms.get(i);
            if (m.getMoveSet() == false) {
                m.moveToTree();
            }

            if (m.isVisible()) {
                m.move();
            } else {
                ms.remove(i);
            }
        }
    }
    // todo add spawn button for people and foresters and gatherers
    // todo add in gatheres for food
    //todo add in animals that can hurt people
    // todo add in buildin function?
    private void updateForesters() {

        ArrayList<Forester> fs = house.getForesters();

        for (int i = 0; i < fs.size(); i++) {

            Forester f = fs.get(i);
            if (f.getOnTask() == false) {
                f.plantTreesMove();
            }

            if (f.isVisible()) {
                f.move();
            } else {
                fs.remove(i);
            }
        }
    }

    private void updateTrees() {

        for (int i = 0; i < trees.size(); i++) {

            Tree m = trees.get(i);

            if (m.isVisible()) {

            } else {
                trees.remove(i);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        inGame();
        checkCollisions();

        updatePeople();
        updateForesters();
        updateTrees();
        repaint();



    }

    private void inGame() {

        if (!ingame) {
            timer.stop();
        }
    }

    public static void addTree(Tree m) {
        trees.add(m);
    }
}



