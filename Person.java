


import java.math.*;
import java.util.ArrayList;

public class Person extends Sprite {

    private final int BOARD_WIDTH = 1500;
    private final int MISSILE_SPEED = 2;
    private double lowDis = 0;
    private int moveX;
    private int moveY;
    private int moveXOld;
    private int moveYOld;
    int personNo;

    private boolean moveSet = false;
    private boolean withTree = false;
    private boolean houseTouch = false;

    public Person(int x, int y, int z) {
        super(x, y);
        personNo = z;
        initPerson();
    }

    private void initPerson() {

        loadImage("person.png");
        getImageDimensions();
    }

    public void move() {

        if (moveX > x) {
            x += MISSILE_SPEED;

        }


        if (moveX < x) {
            x -= MISSILE_SPEED;
        }



        if (moveY < y) {
            y -= MISSILE_SPEED;
        }



        if(moveY > y) {
            y += MISSILE_SPEED;
        }

        if (x > BOARD_WIDTH)
            vis = false;

        if (moveX == x & moveY == y) {
            moveToTree();
        }


    }

    public void moveToTree() {


            ArrayList<Tree> trees = Board.getTrees();
            int num = 0;
            moveXOld = moveX;
            moveYOld = moveY;



            for (Tree m : trees) {



                if (m.isTaken() == false) {

                    int tx = m.getX();
                    int ty = m.getY();
                    int px = this.x;
                    int py = this.y;




                    double dis = Math.sqrt(Math.pow(px - tx, 2) + Math.pow(py - ty, 2));
                    System.out.println("lowDis = " + lowDis + "dis = " + dis);

                    if (lowDis > dis || lowDis == 0) {

                        lowDis = dis;
                        moveX = tx;
                        moveY = ty;

                        System.out.println(lowDis);

                    }



                }




            }
        lowDis = 0;

 // todo fix the ai for knowing if a tree is taken already
            for (Tree m : trees) {



                if (moveX == m.getX() && moveY == m.getY()) {
                 m.setTakenTrue();
                    m.loadImage("TreeMarked.png");
                    m.setChoppableBy(personNo);

                }
            }







            moveSet = true;
        if (moveXOld == moveX && moveYOld == moveY) {
            moveSet = false;
        }


    }

    public void moveToHouse() {



            moveX = 120;
            moveY = 120;




    }

    public void setMoveSetFalse() {
        moveSet = false;
    }

    public void setWithTreeTrue() {
        withTree = true;
    }

    public void setWithTreeFalse() {
        withTree = false;
    }

    public boolean getMoveSet() {
        return moveSet;
    }

    public boolean getWithTree() {
        return withTree;
    }

    public void setHouseTouchTrue() {
        houseTouch = true;
    }

    public void setHouseTouchFalse() {
        houseTouch = false;
    }

    public boolean getHouseTouch() {
        return houseTouch;
    }

    public int getPersonNo() {
        return personNo;
    }
}

// todo add in AI to bring trees back to base