

public class Tree extends Sprite {

    private final int INITIAL_X = 50;
    private boolean taken = false;
    private int treeNumber;
    private int choppableBy = 0;

    public Tree(int x, int y ) {
        super(x, y);



        initTree();
    }

    private void initTree() {


        loadImage("Tree.png");
        getImageDimensions();
    }

    public void setTakenTrue() {
        taken = true;
    }

    public boolean isTaken() {
        return taken;
    }

    public int getTreeNumber() {
        return treeNumber;
    }

    public void setChoppableBy(int i) {
        choppableBy = i;
    }

    public int getChoppableBy() {
        return choppableBy;
    }
}