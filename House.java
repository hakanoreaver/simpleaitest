

import java.util.ArrayList;

public class House extends Sprite{

    int personNo = 1;

    private ArrayList<Person> people;
    private ArrayList<Forester> foresters;


    public House(int x, int y) {
        super(x, y);
        vis = true;

        initHouse();
    }

    private void initHouse() {

        people = new ArrayList<>();
        foresters = new ArrayList<>();
        loadImage("House.png");
        getImageDimensions();
    }


    public ArrayList getPeople() {
        return people;
    }

    public void spawnPerson() {

        people.add(new Person(x + width + 5, y + height + 5, personNo) );
        personNo++;
    }

    public void spawnForester() {

        foresters.add(new Forester(x + width + 5, y + height + 5));
    }

    public ArrayList getForesters() {
        return foresters;
    }
}