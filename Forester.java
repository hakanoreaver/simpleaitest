

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;



public class Forester extends Sprite {

    private final int MISSILE_SPEED = 1;
    private int moveX;
    private int moveY;
    private boolean onTask = false;

    public Forester(int x, int y) {
        super(x, y);
        initForester();

    }

    private void initForester() {
        loadImage("Forester.png");
        getImageDimensions();
    }


    public void move() {

        if (moveX > x) {
            x += MISSILE_SPEED;

        }


        if (moveX < x) {
            x -= MISSILE_SPEED;
        }


        if (moveY < y) {
            y -= MISSILE_SPEED;
        }


        if (moveY > y) {
            y += MISSILE_SPEED;
        }




        if (moveX == x & moveY == y) {

            plantTree();
            onTask = false;

        }
    }
    public void plantTreesMove() {


            moveX = ThreadLocalRandom.current().nextInt(0, 1001);
            moveY = ThreadLocalRandom.current().nextInt(0, 501);

            onTask = true;

    }

    public void plantTree() {
        Tree tree = new Tree(x,y);
        Board.addTree(tree);
    }

    public boolean getOnTask() {
        return onTask;
    }

}